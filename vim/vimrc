" Plugins
call plug#begin('~/.vim/plugged')

	Plug 'junegunn/fzf.vim'
	Plug 'preservim/nerdtree'
    Plug 'stephpy/vim-php-cs-fixer'
    Plug 'maksimr/vim-jsbeautify'
    Plug 'vim-scripts/matchit.zip'
    Plug 'airblade/vim-gitgutter'
    Plug 'c9s/phpunit.vim'
    Plug 'vim-scripts/taglist.vim'

call plug#end()

" set environment path
source ~/.vim_profile

" View configuration
colo simple-dark

highlight LineNr ctermfg=60
syntax on
set rnu
set number
set ruler

set linebreak
set showbreak=+++
set textwidth=120

" Disable arrow keys in command mode
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" Code formatting
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
set autoindent
set smartindent
set backspace=indent,eol,start

" Search configuration
set showmatch
set hlsearch
set ignorecase
set incsearch

" Leader commands
let mapleader = "\<Space>"
" No highlighting until next search
nnoremap <leader>nh  :noh<CR>

"  	Files (runs $FZF_DEFAULT_COMMAND if defined)
nnoremap <leader>f      :Files<CR>
" Git files (git ls-files)
nnoremap <leader>g      :GFiles<CR>
" Git files (git status)
nnoremap <leader>G      :GFiles?<CR>
" Commands
nnoremap <leader>c		:Commands<CR>
" Color schemes
nnoremap <leader>C		:Colors<CR>
" Open buffers
nnoremap <leader>w		:Buffers<CR>
" Windows
nnoremap <leader>W		:Windows<CR>
" :Lines [QUERY] 	Lines in loaded buffers
nnoremap <leader>l		:Lines<CR>
" :BLines [QUERY] 	Lines in the current buffer
nnoremap <leader>L      :BLines<CR>
" :Ag [PATTERN] 	ag search result (ALT-A to select all, ALT-D to deselect all)
nnoremap <leader>ag     :Ag! <C-R><C-W><CR>
" :History 	v:oldfiles and open buffers
nnoremap <leader>oh      :History<CR>
" :History: 	Command history
nnoremap <leader>ch      :History:<CR>
" Toggle Nerdtree / file explorer
nnoremap <leader>ne		:NERDTreeToggle<CR>

" Run php fixer on file
nnoremap <leader>pc    :call PhpCsFixerFixFile()<CR>

" Run CSS beautify
nnoremap <leader>bc    :call CSSBeautify()<cr>
" Run JS beautify
nnoremap <leader>bj    :call JsBeautify()<cr>
" Run HTML beautify
nnoremap <leader>bh    :call HtmlBeautify()<cr>

" let g:gitgutter_enabled = 0
" Gitgutter toggle
nnoremap <leader>gg     :GitGutterToggle<CR>
" Gitgutter buffer toggle
nnoremap <leader>gt     :GitGutterBufferToggle<CR>
" Gitgutter line toggle
nnoremap <leader>gh     :GitGutterLineHighlightsToggle<CR>
" stage hunk
nnoremap <leader>ga     :GitGutterStageHunk<CR>
" unstage hunk
nnoremap <leader>gr     :GitGutterUndoHunk<CR>




" Built in useful commands
"
" Gitgutter
" jump to next hunk (change): ]c, jump to previous hunk (change): [c
"
" Phpunit
" <leader>tf - Run current test case class
" <leader>ta - Run all test cases
" <leader>ts - Switch between source & test file

" Tag list plugin shows class meta
nnoremap <leader>fe :TlistToggle<CR>
